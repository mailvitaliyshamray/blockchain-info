export default {
  data () {
    return {}
  },
  methods: {
    getStorageByName (name) {
      return JSON.parse(localStorage.getItem(name))
    },
    addNewStage (name, data) {
      localStorage.setItem(name, JSON.stringify(data))
    },
    getBTC (value) {
      var m = Math.pow(10, 8)
      let decimal = value * 0.00000001
      let formatNumber = Math.round(decimal * m) / m
      return formatNumber.toFixed(8)
    },
    calculateSum (sum, value) {
      var m = Math.pow(10, 8)
      let formatSum = (Math.round(value * m) + Math.round(sum * m)) / m
      return formatSum.toFixed(8)
    },
    getNumber (value) {
      let regex = /\d+/i
      let result = String(value).match(regex)
      return result !== null ? Number(result[0]) : 0
    },
    isFirstValueMore (a, b) {
      return a > b
    },
    subtractionResult (a, b) {
      return a - b
    },
    getNumberItemOfArray (data) {
      return Array.isArray(data) ? data.length - 1 : 0
    },
    isStorageExists (name) {
      return JSON.parse(localStorage.getItem(name)) !== null
    },
    getClone (arrObj) {
      return JSON.parse(JSON.stringify(arrObj))
    },
    checkElementAvailability (arr, val) {
      return arr.some(arrVal => val === arrVal)
    }
  }
}
