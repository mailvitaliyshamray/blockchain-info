
export default new function () {
  const blockElementParameters = [
    { id: 0, title: 'Block A', x: 10, y: 10, w: 300, h: 100, selected: false, resize: false },
    { id: 1, title: 'Block B', x: 10, y: 390, w: 300, h: 100, selected: false, resize: false },
    { id: 2, title: 'Block C', x: 200, y: 200, w: 300, h: 100, selected: false, resize: false },
    { id: 3, title: 'Block D', x: 390, y: 10, w: 300, h: 100, selected: false, resize: false },
    { id: 4, title: 'Block E', x: 390, y: 390, w: 300, h: 100, selected: false, resize: false }
  ]

  this.getElementslist = function () {
    return blockElementParameters
  }
}()
