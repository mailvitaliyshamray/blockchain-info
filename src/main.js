import Vue from 'vue'
import App from './App.vue'
import iView from 'view-design'
import locale from 'view-design/dist/locale/en-US'
import 'view-design/dist/styles/iview.css'
import './assets/customize/index.css'
import router from './router'
import VueNativeSock from 'vue-native-websocket'
import store from './services/Store.js'
import {
  SOCKET_ONOPEN,
  SOCKET_ONCLOSE,
  SOCKET_ONERROR,
  SOCKET_ONMESSAGE,
  SOCKET_RECONNECT,
  SOCKET_RECONNECT_ERROR
} from './services/MutationTypes'

const mutations = {
  SOCKET_ONOPEN,
  SOCKET_ONCLOSE,
  SOCKET_ONERROR,
  SOCKET_ONMESSAGE,
  SOCKET_RECONNECT,
  SOCKET_RECONNECT_ERROR
}
Vue.use(VueNativeSock, 'wss://ws.blockchain.info/inv', {
  format: 'json',
  store: store,
  mutations: mutations
})

Vue.use(iView, {locale})
// new Vue({ el: '#app', render: h => h(App) })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
