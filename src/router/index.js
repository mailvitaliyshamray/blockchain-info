import Vue from 'vue'
import Router from 'vue-router'
import IndexPage from '@/components/pages/IndexPage'
import InteractiveDesktop from '@/components/pages/InteractiveDesktop'
import BitcoinInformation from '@/components/pages/BitcoinInformation'

Vue.use(Router)

const router = new Router({
  routes: [{
      path: '/interactiveDesktop',
      name: 'interactiveDesktop',
      component: InteractiveDesktop
    },
    {
      path: '/bitcoinInformation',
      name: 'bitcoinInformation',
      component: BitcoinInformation
    }
  ]
})

export default router

if (process.env.NODE_ENV === 'development') {
  router.addRoutes([{
    path: '/',
    name: 'indexPage',
    component: IndexPage
  }])
}
else {
  router.addRoutes([{
    path: '/',
    redirect: '/IndexPage'
  }])
}
